//
//  DataModel.swift
//  
//
//  Created by Jaesung on 2019/12/08.
//

import UIKit
import PencilKit

@available(iOS 13.0, *)
public struct DataModel: Codable {
    /**
    Default name of drawing
     */
    public static let defaultDrawingNames: [String] = ["Sketch"]
    
    /**
     The width used for drawing canvases.
     */
    public static let canvasWidth: CGFloat = 414
    
    /**
     Stored drawings
     */
    public var drawings: [PKDrawing] = []
}
