# DrawingDataKit


This DrawingDataKit provides data model and protocol for storing `PKDrawing` values.

You have to use this package with `PencilKit`


## Reference
- Apple.Inc

## Copyright

© 2019 Jaesung. All Rights Reserved.

## Contact

**Jaesung**

chic0815@icloud.com

## Supporting

> **Supported Platform**: iOS 13.0 or later

> **Versions of Swift**: Swift 5.0 or later

## How to use

### How to import Swift Package Manager
> **URL**: git@gitlab.com:chic0815/drawingdatakit-swift-public.git

Go to Project > Swift Package

Press + button

Enter package repository URL (git@gitlab.com:chic0815/drawingdatakit-swift-public.git)

Select Version and check the version(You can check current version on "Version History")

Go to file in where you want to import `DrawingDataKit`

```Swift
import DrawingDataKit
```


### How to observe changes to data model

First, inherit `DataModelObserver` protocol to the class that you want to observe changes to the data model.

Then, declare new property with `DrawManager` type and append the class to the observer.

```Swift
var drawManager = DrawManager()
self.drawManager.observers.append(self)
```

Finally, Implement protocol method to invoke when the data model has been changed.

```Swift
func didDataModelChange() {
    self.collectionView.reloadData()
}
```


### How to manage thumbnails of my drawings

The class that you want to show thumbnails must inherit `DataModelObserver`

```Swift
class ThumbnailCollectionViewController: UIColletionViewController, DataModelObserver
```

The class must contain `DrawManager` as a property to manage data model.

```Swift
var drawManager = DrawManager()
```

Then, you must append the class to the `dataModelManager.observers` to observe changes to the data model.

```Swift
self.drawManager.observers.append(self)
```

You also need to inform the data model of the current thumbnail traits.
```Swift
self.drawManager.thumbnailTraitCollection = traitCollection
```

```Swift
override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    self.drawManager.thumbnailTraitCollection = traitCollection
}
```

Implement protocol method to invoke when the data model has been changed. (Refer to upper guide: *How to observe changes to data model*)


### Start new drawing

Invoke when you create a new drawing.

```Swift
DrawManager.draw()
```

You have to pass current `DrawManager` to a view controller containing canvas(`PKCanvas`)

```Swift
CanvasViewController.drawManager = self.drawManager
```


### Drawing on `PKCanvasView` 

#### When the view appear
Update `PKCanvasView.drawing` with `DrawManager.drawings[]` when the view will appear.

```Swift
self.canvas.drawing = self.drawManager.drawings[drawIndex]
```

#### When the view disappear
Before the view disappear, you have to update drawing to `DrawManager` if it has been modified.

```Swift
if hasModifiedDrawing {
    self.drawManager.updateDrawing(canvasView.drawing, at: drawingIndex)
}
```


## Version History

1.0.1

> Dec 9, 2019

1.0.0

> Dec 8, 2019
