//
//  DataModelObserver.swift
//  
//
//  Created by Jaesung on 2019/12/08.
//

public protocol DataModelObserver {
    /**
     Invoked when the data model has been changed.
     */
    func didDataModelChange()
}
