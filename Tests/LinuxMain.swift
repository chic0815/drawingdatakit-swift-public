import XCTest

import DrawingDataKitTests

var tests = [XCTestCaseEntry]()
tests += DrawingDataKitTests.allTests()
XCTMain(tests)
