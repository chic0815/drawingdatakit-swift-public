//
//  DataModelManager.swift
//  
//
//  Created by Jaesung on 2019/12/08.
//

import UIKit
import PencilKit
import os

@available(iOS 13.0, *)
public class DrawManager {
    // MARK: - Dispatch Queue
    private let thumbnailQueue = DispatchQueue(label: "ThumbnailQueue", qos: .background)
    private let serializationQueue = DispatchQueue(label: "SerializationQueue", qos: .background)
    
    // MARK: - Public Properties
    public var dataModel = DataModel()
    
    public var thumbnails: [UIImage] = []
    public var thumbnailTraitCollection = UITraitCollection() {
        didSet {
            if oldValue.userInterfaceStyle != thumbnailTraitCollection.userInterfaceStyle {
                self.generateAllThumbnails()
            }
        }
    }
    
    public var observers: [DataModelObserver] = []
    
    public static let thumbnailSize = CGSize(width: 192, height: 256)
    
    public var drawings: [PKDrawing] {
        get { dataModel.drawings }
        set { dataModel.drawings = newValue }
    }
    
    init() {
        self.loadDataModel()
    }
    
    // MARK: - Public Methods
    public func updateDrawing(_ drawing: PKDrawing, at index: Int) {
        self.dataModel.drawings[index] = drawing
        self.generateThumbnail(index)
        self.saveDataModel()
    }

    /**
     Create a new drawing in the data model.
     */
    public func draw() {
        let newDrawing = PKDrawing()
        self.dataModel.drawings.append(newDrawing)
        self.thumbnails.append(UIImage())
        self.updateDrawing(newDrawing, at: dataModel.drawings.count - 1)
    }
    
    // MARK: - Protocol
    private func didChange() {
        for observer in self.observers {
            observer.didDataModelChange()
        }
    }
    
    
    // MARK: - Thumbnails
    private func generateAllThumbnails() {
        for index in drawings.indices {
            generateThumbnail(index)
        }
    }
    
    private func generateThumbnail(_ index: Int) {
        let drawing = drawings[index]
        let aspectRatio = DrawManager.thumbnailSize.width / DrawManager.thumbnailSize.height
        let thumbnailRect = CGRect(x: 0, y: 0, width: DataModel.canvasWidth, height: DataModel.canvasWidth / aspectRatio)
        let thumbnailScale = UIScreen.main.scale * DrawManager.thumbnailSize.width / DataModel.canvasWidth
        let traitCollection = thumbnailTraitCollection
        
        thumbnailQueue.async {
            traitCollection.performAsCurrent {
                let image = drawing.image(from: thumbnailRect, scale: thumbnailScale)
                DispatchQueue.main.async {
                    self.updateThumbnail(image, at: index)
                }
            }
        }
    }
    
    private func updateThumbnail(_ image: UIImage, at index: Int) {
        self.thumbnails[index] = image
        self.didChange()
    }
    
    
    // MARK: - Save Data
    private var saveURL: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths.first!
        return documentsDirectory.appendingPathComponent("DrawingDataKit.data")
    }
    
    private func saveDataModel() {
        let savingDataModel = dataModel
        let url = saveURL
        serializationQueue.async {
            do {
                let encoder = PropertyListEncoder()
                let data = try encoder.encode(savingDataModel)
                try data.write(to: url)
            } catch {
                print("[Debug] \(#function) has been failed because of \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - Load Data
    
    private func loadDataModel() {
        let url = self.saveURL
        serializationQueue.async {
            // Load the data model, or the initial test data.
            let dataModel: DataModel
            
            if FileManager.default.fileExists(atPath: url.path) {
                do {
                    let decoder = PropertyListDecoder()
                    let data = try Data(contentsOf: url)
                    dataModel = try decoder.decode(DataModel.self, from: data)
                } catch {
                    print("[Debug] \(#function) has been failed because of \(error.localizedDescription)")
                    dataModel = self.loadDefaultDrawings()
                }
            } else {
                dataModel = self.loadDefaultDrawings()
            }
            
            DispatchQueue.main.async {
                self.setLoadedDataModel(dataModel)
            }
        }
    }
    
    private func loadDefaultDrawings() -> DataModel {
        var testDataModel = DataModel()
        for sampleDataName in DataModel.defaultDrawingNames {
            guard let data = NSDataAsset(name: sampleDataName)?.data else { continue }
            if let drawing = try? PKDrawing(data: data) {
                testDataModel.drawings.append(drawing)
            }
        }
        return testDataModel
    }
    
    /// Helper method to set the current data model to a data model created on a background queue.
    private func setLoadedDataModel(_ dataModel: DataModel) {
        self.dataModel = dataModel
        self.thumbnails = Array(repeating: UIImage(), count: dataModel.drawings.count)
        self.generateAllThumbnails()
    }
    
}
